package br.com.avenuecode.repository;

import org.h2.server.web.WebServlet;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"br.com.avenuecode.entity"})
@EnableJpaRepositories(basePackages = {"br.com.avenuecode.repository"}, considerNestedRepositories = true)
@ComponentScan(basePackages={"br.com.avenuecode.*"})
@EnableTransactionManagement
public class RepositoryConfiguration {
	
    @Bean
    ServletRegistrationBean h2servletRegistration(){

        ServletRegistrationBean registrationBean = new ServletRegistrationBean( new WebServlet());

        registrationBean.addUrlMappings("/console/*");

        return registrationBean;

    }
    

}
