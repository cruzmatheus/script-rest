package br.com.avenuecode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.avenuecode.entity.Word;

public interface WordRepository extends JpaRepository<Word, Long> {
	
	@Query(value="SELECT * FROM Word w WHERE w.id_character = ?1 ORDER BY w.count DESC LIMIT 10", nativeQuery=true)
	public List<Word> findByCharacter(long idCharacter);

}
