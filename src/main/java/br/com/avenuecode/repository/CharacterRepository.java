package br.com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.avenuecode.entity.Character;

public interface CharacterRepository extends JpaRepository<Character, Long> {
	

}
