package br.com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.avenuecode.entity.Dialog;

public interface DialogRepository extends JpaRepository<Dialog, Long> {

}
