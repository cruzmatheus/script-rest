package br.com.avenuecode;

public class Message {
	
	private int status;
	private String text;
	
	private Message(int status, String text) {
		this.status = status;
		this.text = text;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public static Message buildSuccessfulMessage() {
		return new Message(200, "Movie script successfully received");
	}
	public static Message buildAlreadyReceivedMessage() {
		return new Message(403, "Movie script already received");
	}
	public static Message buildErrorMessage() {
		return new Message(500, "Unexpected error");
	}
	
	

}
