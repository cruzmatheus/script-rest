package br.com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.avenuecode.Message;
import br.com.avenuecode.entity.Dialog;
import br.com.avenuecode.service.DialogService;
import br.com.avenuecode.service.DialogServiceBean;

@RestController
public class DialogController {
	
	private DialogServiceBean dialogServiceBean;
	
	@Autowired
	private DialogService dialogService;
	
	public DialogController() {
		dialogServiceBean = new DialogServiceBean();
	}

    @RequestMapping(value="/script",method=RequestMethod.POST)
    public Message createScript(@RequestBody String script) {
    	List<Dialog> dialogs = null;
    	Message message = Message.buildSuccessfulMessage();
    	try {
    		dialogs = dialogServiceBean.receiveScript(script);
    		
    		for (Dialog dialog : dialogs) {
				dialogService.saveDialog(dialog);
			}
    		
		} catch (Exception e) {
			e.printStackTrace();
			message = Message.buildErrorMessage();
			return null;
		}
    	
        return message;
    }
    
    @RequestMapping(value="/settings", method=RequestMethod.GET)
    public List<Dialog> getScripts() {
    	try {
    		return dialogService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
    
    @RequestMapping(value="/settings/{id}", method=RequestMethod.GET)
    public Dialog getDialog(@PathVariable long id) {
    	try {
			return dialogService.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
}