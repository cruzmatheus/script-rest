package br.com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.avenuecode.entity.Character;
import br.com.avenuecode.service.CharacterService;
import br.com.avenuecode.service.CharacterServiceBean;

@RestController
public class CharacterController {
	
	@Autowired
	private CharacterService characterService;
	
	@Autowired
	private CharacterServiceBean characterServiceBean;
	
	@RequestMapping(value="/characters", method=RequestMethod.GET)
	public List<Character> getCharacters() {
		try {
			return characterServiceBean.getCharacters();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/characters/{id}", method=RequestMethod.GET)
	public Character getCharacter(@PathVariable long id) {
		try {
			return characterServiceBean.getCharacter(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
