package br.com.avenuecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.avenuecode.repository.RepositoryConfiguration;

@EnableJpaRepositories
@SpringBootApplication
@SpringApplicationConfiguration(classes= {RepositoryConfiguration.class})
public class Application  {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


}
