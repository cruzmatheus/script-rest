package br.com.avenuecode.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Character implements Serializable {
	
	private static final long serialVersionUID = 9098357633320460819L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "character", fetch=FetchType.EAGER)
	@JsonProperty(value="wordCount")
	private List<Word> words = new ArrayList<>();
	@ManyToOne
	@JoinColumn(name = "id_dialog", referencedColumnName = "id")
	@JsonIgnore
	private Dialog dialog;
	
	public Character(String name) {
		this.name = name;
	}
	
	public Character() { }
	
	public void addWords(String line) {
		String[] newWords = line.replaceAll("\\p{P}", "").split(" ");
		for (String word : newWords) {
			Optional<Word> wordOptional = words.stream().filter(w -> w.getWord().equals(word)).findAny();
			if (!wordOptional.isPresent()) {
				words.add(new Word(word, this));
			} else {
				wordOptional.get().addCount();
			}
		}
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Dialog getDialog() {
		return dialog;
	}
	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dialog == null) ? 0 : dialog.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Character other = (Character) obj;
		if (dialog == null) {
			if (other.dialog != null)
				return false;
		} else if (!dialog.equals(other.dialog))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public List<Word> getWords() {
		return words;
	}

	public void setWords(List<Word> words) {
		this.words = words;
	}
	
	

}
