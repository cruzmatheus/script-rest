package br.com.avenuecode.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Word implements Serializable {
	
	private static final long serialVersionUID = 6976963689554634889L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	private long id;
	private String word;
	private int count;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_character", referencedColumnName = "id")
	@JsonIgnore
	private Character character;
	
	public Word(String word, Character character) {
		this.word = word;
		this.character = character;
		addCount();
	}
	
	public Word() { }
	
	public void addCount() {
		this.count += 1;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public Character getCharacter() {
		return character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	@Override
	public String toString() {
		return "Word [word=" + word + ", count=" + count + "]";
	}

	
	

}
