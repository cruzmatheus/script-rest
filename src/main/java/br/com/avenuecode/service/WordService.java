package br.com.avenuecode.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import br.com.avenuecode.entity.Word;

public interface WordService {

	public List<Word> findByCharacter(long idCharacter);
}
