package br.com.avenuecode.service;

import java.util.List;

import br.com.avenuecode.entity.Dialog;

public interface DialogService {
	public List<Dialog> findAll();
	public void saveDialog(Dialog dialog);
	public Dialog findById(long id);
}
