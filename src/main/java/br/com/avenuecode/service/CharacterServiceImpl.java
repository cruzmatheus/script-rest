package br.com.avenuecode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.avenuecode.repository.CharacterRepository;
import br.com.avenuecode.entity.Character;

@Service
@Transactional
public class CharacterServiceImpl implements CharacterService {
	
	@Autowired
	private CharacterRepository characterRepository;

	@Override
	public List<Character> findAll() {
		return characterRepository.findAll();
	}

	@Override
	public Character findById(long id) {
		return characterRepository.findOne(id);
	}

}
