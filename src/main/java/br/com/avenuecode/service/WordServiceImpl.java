package br.com.avenuecode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.avenuecode.entity.Word;
import br.com.avenuecode.repository.WordRepository;

@Service
@Transactional
public class WordServiceImpl implements WordService {

	@Autowired
	private WordRepository wordRepository;
	
	@Override
	public List<Word> findByCharacter(long idCharacter) {
		return wordRepository.findByCharacter(idCharacter);
	}

}
