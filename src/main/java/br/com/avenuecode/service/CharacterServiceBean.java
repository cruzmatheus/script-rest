package br.com.avenuecode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.avenuecode.entity.Character;

@Component
public class CharacterServiceBean {
	
	@Autowired
	private CharacterService characterService;
	
	@Autowired
	private WordService wordService;
	
	public List<Character> getCharacters() {
		List<Character> characters = characterService.findAll();
		
		for (Character character : characters) {
			character.setWords(wordService.findByCharacter(character.getId()));
		}
		
		return characters;
	}
	
	public Character getCharacter(long id) {
		Character character = characterService.findById(id);
		character.setWords(wordService.findByCharacter(character.getId()));
		
		return character;
	}

}
