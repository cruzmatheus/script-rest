package br.com.avenuecode.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.avenuecode.entity.Character;
import br.com.avenuecode.entity.Dialog;

public class DialogServiceBean {
	
	List<Dialog> dialogs;
	
	public List<Dialog> receiveScript(String script) throws IOException {
		dialogs = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new StringReader(script));
		Dialog dialog = null;
		Character character = null;
		String line = "";
		String title = "";
		while ((line = reader.readLine()) != null) {
			if (isSceneHeading(line)) {
				title = extractDialogueTitle(line);
				dialog = getDialogue(title);
				dialogs.add(dialog);
			} else if (isCharacter(line)) {
				character = extractCharacter(line, dialog);
				dialog.addCharacter(character);
			} else if (isDialogue(line)) {
				character.addWords(line.trim());
			}
		}
		return dialogs;
	}
	
	private Dialog getDialogue(String name) {
		for (Dialog d : dialogs) {
			if (d.getName().equalsIgnoreCase(name))
				return d;
		}
		
		return new Dialog(name);
	}
	
	public String extractDialogueTitle(String line) {
		String title = "";
		int endingChar = line.indexOf('.')+1;
		String temp = line.substring(endingChar, line.length()-1);
		title = temp.split("-")[0].trim();
		return title;
	}
	
	private boolean isSceneHeading(String line) {
		return line.startsWith("EXT.")
				|| line.startsWith("INT.")
				|| line.startsWith("INT./EXT.");
	}
	
	private boolean isCharacter(String line) {
		return line.matches(" {22}[^ ]*");
	}
	
	private boolean isDialogue(String line) {
		return line.matches("( {10}[^ ]*)([\\w]+[\\s]*[\\W]*)*");
	}
	
	private Character extractCharacter(String line, Dialog dialog) {
		String name = line.trim();
		Character character = null;
		
		Optional<Character> characterOptional = dialog.getCharacters().stream().filter(c -> c.getName().equals(name)).findAny();
		if (characterOptional.isPresent()) {
			character = characterOptional.get();
		} else {
			character = new Character(name);
			character.setDialog(dialog);
		}
		
		return character;
	}

}
