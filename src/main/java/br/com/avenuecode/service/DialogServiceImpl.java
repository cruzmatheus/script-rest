package br.com.avenuecode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.avenuecode.entity.Dialog;
import br.com.avenuecode.repository.DialogRepository;

@Service
@Transactional
public class DialogServiceImpl implements DialogService {
	
	@Autowired
	private DialogRepository dialogRepository;

	@Override
	public List<Dialog> findAll() {
		return dialogRepository.findAll();
	}

	@Override
	public void saveDialog(Dialog dialog) {
		dialogRepository.save(dialog);
	}

	@Override
	public Dialog findById(long id) {
		return dialogRepository.findOne(id);
	}

}
