package br.com.avenuecode.service;

import java.util.List;

import br.com.avenuecode.entity.Character;

public interface CharacterService {
	
	public List<Character> findAll();
	public Character findById(long id);

}
